import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelConsumptionPage } from './fuel-consumption';
import { IonPullupModule } from 'ionic-pullup';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FuelConsumptionPage,
  ],
  imports: [
    IonicPageModule.forChild(FuelConsumptionPage),
    IonPullupModule,
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class FuelConsumptionPageModule {}
