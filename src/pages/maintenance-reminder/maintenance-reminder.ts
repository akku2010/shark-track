import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import * as moment from 'moment';

import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-maintenance-reminder',
  templateUrl: 'maintenance-reminder.html',
})
export class MaintenanceReminderPage implements OnInit {
  islogin: any;
  portstemp: any[] = [];
  selectedVehicle: any;
  reminderType: string;
  reminderTypes: any = [];
  datetimeStart: any;
  datetimeEnd: any;
  remData: any[] = [];
  isParamData: boolean = false;
  // twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().add(5, 'year').format("YYYY-MM-DD");
  twoMonthsLater: any = moment().subtract(5, 'year').format("YYYY-MM-DD");

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    private modalCtrl: ModalController,
    private toastCtrl: ToastController,
    private translate: TranslateService
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // console.log("email => " + this.islogin._id);

    this.reminderTypes = [{
      viewValue: this.translate.instant("Service"),
      value: "Service",
    },
    {
      viewValue: this.translate.instant("Oil Change"),
      value: "Oil Change",
    },
    {
      viewValue: this.translate.instant("Tyres"),
      value: "Tyres",
    },
    {
      viewValue: this.translate.instant("Maintenance"),
      value: "Maintenance",
    },
    {
      viewValue: this.translate.instant("Auto Repair"),
      value: "Auto Repair",
    },
    {
      viewValue: this.translate.instant("Body Work"),
      value: "Body Work",
    },
    {
      viewValue: this.translate.instant("Diagnostics"),
      value: "Diagnostics",
    },
    {
      viewValue: this.translate.instant("Tune Up"),
      value: "Tune Up",
    },
    {
      viewValue: this.translate.instant("Brake Job"),
      value: "Brake Job",
    },
    {
      viewValue: this.translate.instant("Oil & Oil Filter Change"),
      value: "Oil & Oil Filter Change",
    },
    {
      viewValue: this.translate.instant("Tyer Care"),
      value: "Tyer Care",
    },
    {
      viewValue: this.translate.instant("Towing"),
      value: "Towing",
    },
    {
      viewValue: this.translate.instant("Wheel Balance & Alignment"),
      value: "Wheel Balance & Alignment",
    },
    {
      viewValue: this.translate.instant("Fleet"),
      value: "Fleet",
    },
    {
      viewValue: this.translate.instant("Auto Tracking"),
      value: "Auto Tracking",
    },
    {
      viewValue: this.translate.instant("A/C Repair"),
      value: "A/C Repair",
    },
    {
      viewValue: this.translate.instant("Others"),
      value: "Others",
    }
    ];
    // this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeStart = moment().subtract(1, 'months').format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('end date', this.datetimeEnd);

    if (navParams.get('param')) {
      this.isParamData = true;
      console.log('reminder navparams: ', navParams.get('param'));
      this.selectedVehicle = navParams.get('param');
      this.onChangeVehicle();
    }

  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter MaintenanceReminderPage');
  }

  ngOnInit() {
    if (!this.isParamData) {
      this.getdevices();
      this.getReminders();
    }
  }

  onSeletChange(rem) {
    console.log("reminder changed: ", this.reminderType)
    this.remData = [];
    this.getReminders();
  }

  onChangeVehicle() {
    console.log("vehicle changed: ", this.selectedVehicle)
    this.remData = [];
    this.getReminders();
  }

  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }

    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {

        this.portstemp = data.devices;
      },
        err => {

          console.log(err);
        });
  }

  getReminders() {
    const Burl = this.apiCall.mainUrl + 'reminder/reminderdatatable';
    var payload = {};
    if (this.islogin.isSuperAdmin === true) {
      if (this.reminderType != undefined && this.selectedVehicle != undefined) {
        payload = {
          "draw": 2,
          "columns": [
            {
              "data": "_id"
            },
            {
              "data": "user.first_name"
            },
            {
              "data": "user.last_name"
            },
            {
              "data": "device.Device_Name"
            },
            {
              "data": "reminder_date"
            },
            {
              "data": "notification_type.SMS"
            },
            {
              "data": "notification_type.EMAIL"
            },
            {
              "data": "notification_type.PUSH_NOTIFICATION"
            },
            {
              "data": "reminder_type"
            },
            {
              "data": "prior_reminder"
            },
            {
              "data": "note"
            }
          ],
          "order": [
            {
              "column": 0,
              "dir": "asc"
            }
          ],
          "start": 0,
          "length": 25,
          "search": {
            "value": "",
            "regex": false
          },
          "op": {},
          "select": [],
          "find": {
            "$and": [
              {
                "$or": [
                  {
                    "user": this.islogin._id
                  },
                  {
                    "created_by": this.islogin._id
                  },
                  {}
                ]
              },
              {
                "device": {
                  "$in": [
                    this.selectedVehicle._id
                  ]
                }
              },
              {
                "reminder_date": {
                  "$gte": new Date(this.datetimeStart).toISOString(),
                  // "$lte": new Date(this.datetimeEnd).toISOString()
                }
              },
              {
                "reminder_type": this.reminderType
              }
            ]
          }
        };
      } else {
        if (this.reminderType != undefined && this.selectedVehicle == undefined) {
          payload = {
            "draw": 2,
            "columns": [
              {
                "data": "_id"
              },
              {
                "data": "user.first_name"
              },
              {
                "data": "user.last_name"
              },
              {
                "data": "device.Device_Name"
              },
              {
                "data": "reminder_date"
              },
              {
                "data": "notification_type.SMS"
              },
              {
                "data": "notification_type.EMAIL"
              },
              {
                "data": "notification_type.PUSH_NOTIFICATION"
              },
              {
                "data": "reminder_type"
              },
              {
                "data": "prior_reminder"
              },
              {
                "data": "note"
              }
            ],
            "order": [
              {
                "column": 0,
                "dir": "asc"
              }
            ],
            "start": 0,
            "length": 25,
            "search": {
              "value": "",
              "regex": false
            },
            "op": {},
            "select": [],
            "find": {
              "$and": [
                {
                  "$or": [
                    {
                      "user": this.islogin._id
                    },
                    {
                      "created_by": this.islogin._id
                    },
                    {}
                  ]
                },
                {
                  "reminder_date": {
                    "$gte": new Date(this.datetimeStart).toISOString(),
                    // "$lte": new Date(this.datetimeEnd).toISOString()
                  }
                },
                {
                  "reminder_type": this.reminderType
                }
              ]
            }
          };
        } else {
          if (this.reminderType == undefined && this.selectedVehicle != undefined) {
            payload = {
              "draw": 2,
              "columns": [
                {
                  "data": "_id"
                },
                {
                  "data": "user.first_name"
                },
                {
                  "data": "user.last_name"
                },
                {
                  "data": "device.Device_Name"
                },
                {
                  "data": "reminder_date"
                },
                {
                  "data": "notification_type.SMS"
                },
                {
                  "data": "notification_type.EMAIL"
                },
                {
                  "data": "notification_type.PUSH_NOTIFICATION"
                },
                {
                  "data": "reminder_type"
                },
                {
                  "data": "prior_reminder"
                },
                {
                  "data": "note"
                }
              ],
              "order": [
                {
                  "column": 0,
                  "dir": "asc"
                }
              ],
              "start": 0,
              "length": 25,
              "search": {
                "value": "",
                "regex": false
              },
              "op": {},
              "select": [],
              "find": {
                "$and": [
                  {
                    "$or": [
                      {
                        "user": this.islogin._id
                      },
                      {
                        "created_by": this.islogin._id
                      },
                      {}
                    ]
                  },
                  {
                    "device": {
                      "$in": [
                        this.selectedVehicle._id
                      ]
                    }
                  },
                  {
                    "reminder_date": {
                      "$gte": new Date(this.datetimeStart).toISOString(),
                      // "$lte": new Date(this.datetimeEnd).toISOString()
                    }
                  }
                ]
              }
            };
          } else {
            if (this.reminderType == undefined && this.selectedVehicle == undefined) {
              payload = {
                "draw": 2,
                "columns": [
                  {
                    "data": "_id"
                  },
                  {
                    "data": "user.first_name"
                  },
                  {
                    "data": "user.last_name"
                  },
                  {
                    "data": "device.Device_Name"
                  },
                  {
                    "data": "reminder_date"
                  },
                  {
                    "data": "notification_type.SMS"
                  },
                  {
                    "data": "notification_type.EMAIL"
                  },
                  {
                    "data": "notification_type.PUSH_NOTIFICATION"
                  },
                  {
                    "data": "reminder_type"
                  },
                  {
                    "data": "prior_reminder"
                  },
                  {
                    "data": "note"
                  },
                  {
                    "data": "status"
                  }
                ],
                "order": [
                  {
                    "column": 0,
                    "dir": "asc"
                  }
                ],
                "start": 0,
                "length": 25,
                "search": {
                  "value": "",
                  "regex": false
                },
                "op": {},
                "select": [],
                "find": {
                  "$and": [
                    {
                      "$or": [
                        {
                          "user": this.islogin._id
                        },
                        {
                          "created_by": this.islogin._id
                        }
                      ]
                    },
                    {
                      "reminder_date": {
                        "$gte": new Date(this.datetimeStart).toISOString(),
                        // "$lte": new Date(this.datetimeEnd).toISOString()
                      }
                    }
                  ]
                }
              }
            }
          }
        }
      }
    } else {
      if (this.reminderType != undefined && this.selectedVehicle != undefined) {
        payload = {
          "draw": 2,
          "columns": [
            {
              "data": "_id"
            },
            {
              "data": "user.first_name"
            },
            {
              "data": "user.last_name"
            },
            {
              "data": "device.Device_Name"
            },
            {
              "data": "reminder_date"
            },
            {
              "data": "notification_type.SMS"
            },
            {
              "data": "notification_type.EMAIL"
            },
            {
              "data": "notification_type.PUSH_NOTIFICATION"
            },
            {
              "data": "reminder_type"
            },
            {
              "data": "prior_reminder"
            },
            {
              "data": "note"
            }
          ],
          "order": [
            {
              "column": 0,
              "dir": "asc"
            }
          ],
          "start": 0,
          "length": 25,
          "search": {
            "value": "",
            "regex": false
          },
          "op": {},
          "select": [],
          "find": {
            "$and": [
              {
                "$or": [
                  {
                    "user": this.islogin._id
                  },
                  {
                    "created_by": this.islogin.supAdmin
                  }
                ]
              },
              {
                "device": {
                  "$in": [
                    this.selectedVehicle._id
                  ]
                }
              },
              {
                "reminder_date": {
                  "$gte": new Date(this.datetimeStart).toISOString(),
                  // "$lte": "2020-10-31T06:32:00.000Z"
                }
              },
              {
                "reminder_type": this.reminderType
              }
            ]
          }
        };
      } else {
        if (this.reminderType != undefined && this.selectedVehicle == undefined) {
          payload = {
            "draw": 2,
            "columns": [
              {
                "data": "_id"
              },
              {
                "data": "user.first_name"
              },
              {
                "data": "user.last_name"
              },
              {
                "data": "device.Device_Name"
              },
              {
                "data": "reminder_date"
              },
              {
                "data": "notification_type.SMS"
              },
              {
                "data": "notification_type.EMAIL"
              },
              {
                "data": "notification_type.PUSH_NOTIFICATION"
              },
              {
                "data": "reminder_type"
              },
              {
                "data": "prior_reminder"
              },
              {
                "data": "note"
              }
            ],
            "order": [
              {
                "column": 0,
                "dir": "asc"
              }
            ],
            "start": 0,
            "length": 25,
            "search": {
              "value": "",
              "regex": false
            },
            "op": {},
            "select": [],
            "find": {
              "$and": [
                {
                  "$or": [
                    {
                      "user": this.islogin._id
                    },
                    {
                      "created_by": this.islogin.supAdmin
                    }
                  ]
                },
                {
                  "reminder_date": {
                    "$gte": new Date(this.datetimeStart).toISOString(),
                    // "$lte": new Date(this.datetimeEnd).toISOString()
                  }
                },
                {
                  "reminder_type": this.reminderType
                }
              ]
            }
          };
        } else {
          if (this.reminderType == undefined && this.selectedVehicle != undefined) {
            payload = {
              "draw": 2,
              "columns": [
                {
                  "data": "_id"
                },
                {
                  "data": "user.first_name"
                },
                {
                  "data": "user.last_name"
                },
                {
                  "data": "device.Device_Name"
                },
                {
                  "data": "reminder_date"
                },
                {
                  "data": "notification_type.SMS"
                },
                {
                  "data": "notification_type.EMAIL"
                },
                {
                  "data": "notification_type.PUSH_NOTIFICATION"
                },
                {
                  "data": "reminder_type"
                },
                {
                  "data": "prior_reminder"
                },
                {
                  "data": "note"
                }
              ],
              "order": [
                {
                  "column": 0,
                  "dir": "asc"
                }
              ],
              "start": 0,
              "length": 25,
              "search": {
                "value": "",
                "regex": false
              },
              "op": {},
              "select": [],
              "find": {
                "$or": [
                  {
                    "user": this.islogin._id
                  },
                  {
                    "created_by": this.islogin.supAdmin
                  }
                ]
                ,
                "device": {
                  "$in": [
                    this.selectedVehicle._id
                  ]
                },
                "reminder_date": {
                  "$gte": new Date(this.datetimeStart).toISOString(),
                  // "$lte": new Date(this.datetimeEnd).toISOString()
                }

              }
            };
          } else {
            if (this.reminderType == undefined && this.selectedVehicle == undefined) {
              payload = {
                "draw": 2,
                "columns": [
                  {
                    "data": "_id"
                  },
                  {
                    "data": "user.first_name"
                  },
                  {
                    "data": "user.last_name"
                  },
                  {
                    "data": "device.Device_Name"
                  },
                  {
                    "data": "reminder_date"
                  },
                  {
                    "data": "notification_type.SMS"
                  },
                  {
                    "data": "notification_type.EMAIL"
                  },
                  {
                    "data": "notification_type.PUSH_NOTIFICATION"
                  },
                  {
                    "data": "reminder_type"
                  },
                  {
                    "data": "prior_reminder"
                  },
                  {
                    "data": "note"
                  },
                  {
                    "data": "status"
                  }
                ],
                "order": [
                  {
                    "column": 0,
                    "dir": "asc"
                  }
                ],
                "start": 0,
                "length": 25,
                "search": {
                  "value": "",
                  "regex": false
                },
                "op": {},
                "select": [],
                "find": {
                  "$and": [
                    {
                      "$or": [
                        {
                          "user": this.islogin._id
                        },
                        {
                          "created_by": this.islogin.supAdmin
                        }
                      ]
                    },
                    {
                      "reminder_date": {
                        "$gte": new Date(this.datetimeStart).toISOString(),
                        // "$lte": new Date(this.datetimeEnd).toISOString()
                      }
                    }
                  ]
                }
              }
            }
          }
        }
      }
    }
    // let current_date = new Date();
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(Burl, payload)
      .subscribe(resData => {
        this.apiCall.stopLoading();
        console.log("response reminder: " + resData);
        if (resData.data.length > 0) {
          for (var i = 0; i < resData.data.length; i++) {
            debugger
            // let someti = new Date(resData.data[i].reminder_date);
            // if (someti < current_date) {
            //   return;
            // }
            var date = moment(new Date(resData.data[i].reminder_date)).format('YYYY-MM-DD');
            var now = moment().format('YYYY-MM-DD');

            if (now > date) {
              // date is past
              console.log("im past")
            } else {
              console.log("im future")

              // date is future

              var date1 = moment(new Date(resData.data[i].reminder_date), 'DD/MM/YYYY').format("llll");
              var str = date1.split(', ');
              var day = str[0];
              var date3 = str[1].split(' ');
              var date4 = str[2].split(' ');
              var time = date4[1] + ' ' + date4[2];
              var year = date4[0];
              var month = date3[0];
              var dateNum = date3[1];
              this.remData.push({
                datFormats: {
                  'day': day,
                  'time': time,
                  'year': year,
                  'month': month,
                  'dateNum': dateNum
                },
                vehiclName: (resData.data[i].device ? resData.data[i].device.Device_Name : null),
                reminderType: (resData.data[i].reminder_type ? resData.data[i].reminder_type : null),
                notifType: (resData.data[i].notification_type ? resData.data[i].notification_type : null),
                note: (resData.data[i].note ? resData.data[i].note : null),
                prior_reminder: (resData.data[i].prior_reminder ? resData.data[i].prior_reminder : null)
              })
            }
          }
        } else {
          this.toastCtrl.create({
            message: 'Reminders not found...',
            duration: 1500,
            position: 'bottom'
          }).present();
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  onAddReminder() {
    let modal;
    if (this.navParams.get('param')) {
      modal = this.modalCtrl.create('AddReminderPage', {
        param: this.navParams.get('param')
      });
    } else {
      modal = this.modalCtrl.create('AddReminderPage', {
        param: null
      });
    }

    modal.present();
    modal.onDidDismiss(() => {
      this.remData = [];
      this.getReminders();
    })
  }

}
